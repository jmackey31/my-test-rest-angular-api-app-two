'use strict';

var mongoose = require('mongoose'),
  Plant = mongoose.model('PlantList');

/**
 * Find plant by id
 */
exports.plant = function(req, res, next, id) {
  Plant.load(id, function(err, plant) {
    if (err) return next(err);
    if (!plant) return next(new Error('Failed to load plant ' + id));
    req.plant = plant;
    next();
  });
};

/**
 * Create a plant
 */
exports.create = function(req, res) {
  var plant = new Plant(req.body);
  plant.creator = req.user;

  plant.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(plant);
    }
  });
};

/**
 * Update a plant
 */
exports.update = function(req, res) {
  var plant = req.plant;
  plant.title = req.body.title;
  plant.content = req.body.content;
  plant.save(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(plant);
    }
  });
};

/**
 * Delete a plant
 */
exports.destroy = function(req, res) {
  var plant = req.plant;

  plant.remove(function(err) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(plant);
    }
  });
};

/**
 * Show a Plant
 */
exports.show = function(req, res) {
  res.json(req.plant);
};

/**
 * List of plants
 */
exports.all = function(req, res) {
  Plant.find().sort('-created').populate('creator', 'username').exec(function(err, plants) {
    if (err) {
      res.json(500, err);
    } else {
      res.json(plants);
    }
  });
};
