'use strict';

angular.module('angularPassportApp')
  .controller('PlantsCtrl', function ($scope, Plants, $location, $routeParams, $rootScope) {

    $scope.create = function() {
      var plant = new Plants({
        title: this.title,
        content: this.content
      });
      plant.$save(function(response) {
        $location.path("plants/" + response._id);
      });

      this.title = "";
      this.content = "";
    };

    $scope.remove = function(plant) {
      plant.$remove();

      for (var i in $scope.plants) {
        if ($scope.plants[i] == plant) {
          $scope.plants.splice(i, 1);
        }
      }
    };

    $scope.update = function() {
      var plant = $scope.plant;
      plant.$update(function() {
        $location.path('plants/' + plant._id);
      });
    };

    $scope.find = function() {
      Plants.query(function(plants) {
        $scope.plants = plants;
      });
    };

    $scope.findOne = function() {
      Plants.get({
        plantId: $routeParams.plantId
      }, function(plant) {
        $scope.plant = plant;
      });
    };
  });
